package com.streaming1.entity;

import java.io.Serializable;

public class TrackingData implements Serializable {
    String waybill_no = null;
    String consignment_status = null;
    String updated_at = String.valueOf(Long.parseLong(null));

    public String getWaybill_no(){
        return this.waybill_no;
    }

    public String getConsignment_status(){
        return this.consignment_status;
    }

    public String getUpdated_at(){
        return this.updated_at;
    }

    public void setWaybill_no(String waybill_no) {
        this.waybill_no = waybill_no;
    }

    public void setConsignment_status(String consignment_status) {
        this.consignment_status = consignment_status;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
