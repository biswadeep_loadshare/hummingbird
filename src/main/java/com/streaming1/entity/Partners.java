package com.streaming1.entity;

import java.io.Serializable;
import java.math.BigInteger;

public class Partners implements Serializable {
    String name = null;
    BigInteger Id = null;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getId() {
        return Id;
    }

    public void setId(BigInteger id) {
        Id = id;
    }

}
