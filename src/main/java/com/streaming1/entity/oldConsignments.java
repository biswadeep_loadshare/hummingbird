package com.streaming1.entity;

import com.datastax.driver.mapping.annotations.Column;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class oldConsignments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "id")
    private BigInteger id;
    @Column(name = "waybill_no")
    private String waybillNo;
    @Column(name = "location_id")
    private BigInteger locationId;
    @Column(name = "partner_id")
    private BigInteger partnerId;
    @Column(name = "customer_pickup_loc_id")
    private BigInteger customerPickupLocId;
    @Column(name = "customer_id")
    private BigInteger customerId;
    @Column(name = "customer_account_type")
    private String customerAccountType;
    @Column(name = "consignee_id")
    private BigInteger consigneeId;
    @Column(name = "biller_id")
    private BigInteger billerId;
    @Column(name = "shipper_id")
    private BigInteger shipperId;
    @Column(name = "booking_date")
    private Date bookingDate;
    @Column(name = "expected_delivery_date")
    private Date expectedDeliveryDate;
    @Column(name = "pickup_date")
    private Date pickupDate;
    @Column(name = "pickedup_date")
    private Date pickedupDate;
    @Column(name = "booking_office_loc_id")
    private BigInteger bookingOfficeLocId;
    @Column(name = "product_type")
    private String productType;
    @Column(name = "order_ref_no")
    private String orderRefNo;
    @Column(name = "total_weight")
    private BigDecimal totalWeight;
    @Column(name = "total_volumentric_weight")
    private BigDecimal totalVolumentricWeight;
    @Column(name = "total_chargable_weight")
    private BigDecimal totalChargableWeight;
    @Column(name = "total_shipment_count")
    private BigInteger totalShipmentCount;
    @Column(name = "pincode_id")
    private BigInteger pincodeId;
    @Column(name = "lead_id")
    private BigInteger leadId;
    @Column(name = "consignment_status")
    private String consignmentStatus;
    @Column(name = "insurance_info")
    private String insuranceInfo;
    @Column(name = "service_type")
    private String serviceType;
    @Column(name = "payment_type")
    private String paymentType;
    @Column(name = "consignment_mode")
    private String consignmentMode;
    @Column(name = "franchisee_id")
    private BigInteger franchiseeId;
    @Column(name = "last_status_reason_code")
    private String lastStatusReasonCode;
    @Column(name = "description")
    private String description;
    @Column(name = "remarks")
    private String remarks;
    @Column(name = "reschedule_date")
    private Date rescheduleDate;
    @Column(name = "user_id")
    private BigInteger userId;
    // @Column(name = "drs_attempt_count")
    // private BigInteger drsAttemptCount;
    @Column(name = "consignment_amount")
    private BigDecimal consignmentAmount;
    @Column(name = "consignment_currency")
    private String consignmentCurrency;
    @Column(name = "payable_amount")
    private BigDecimal payableAmount;
    @Column(name = "payable_currency")
    private String payableCurrency;
    @Column(name = "created_at")
    private Date createdAt;
    @Column(name = "updated_at")
    private Date updatedAt;
    @Column(name = "is_active")
    private Boolean isActive;
    @Column(name = "parent_consignment_id")
    private BigInteger parentConsignmentId;
    @Column(name = "parent_partner_id")
    private BigInteger parentPartnerId;
    @Column(name = "root_consignment_id")
    private BigInteger rootConsignmentId;
    @Column(name = "root_waybill_no")
    private String rootWaybillNo;
    @Column(name = "root_partner_id")
    private BigInteger rootPartnerId;
    private Date effectiveDate;
    @Column(name = "consignment_type")
    private String consignmentType;
    @Column(name = "docket_type")
    private String docketType;
    @Column(name = "processing_type")
    private String processingType;
    @Column(name = "flow_type")
    private String flowType;
    @Column(name = "is_pod_done")
    private Boolean isPodDone = true;
    @Column(name = "is_eligible_for_rto")
    private Boolean isEligibleForRto;
    @Column(name = "reason_eligible_for_rto")
    private String reasonEligibleForRto;
    @Column(name = "customer_promise_date")
    private Date customerPromiseDate;
    @Column(name = "status_correction_request_id")
    private BigInteger statusCorrectionRequestId;
    @Column(name = "parent_shipper_id")
    private BigInteger parentShipperId;

    public oldConsignments() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getWaybillNo() {
        return waybillNo;
    }

    public void setWaybillNo(String waybillNo) {
        this.waybillNo = waybillNo;
    }

    public BigInteger getLocationId() {
        return locationId;
    }

    public void setLocationId(BigInteger locationId) {
        this.locationId = locationId;
    }

    public BigInteger getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(BigInteger partnerId) {
        this.partnerId = partnerId;
    }

    public BigInteger getCustomerPickupLocId() {
        return customerPickupLocId;
    }

    public void setCustomerPickupLocId(BigInteger customerPickupLocId) {
        this.customerPickupLocId = customerPickupLocId;
    }

    public BigInteger getCustomerId() {
        return customerId;
    }

    public void setCustomerId(BigInteger customerId) {
        this.customerId = customerId;
    }

    public String getCustomerAccountType() {
        return customerAccountType;
    }

    public void setCustomerAccountType(String customerAccountType) {
        this.customerAccountType = customerAccountType;
    }

    public BigInteger getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(BigInteger consigneeId) {
        this.consigneeId = consigneeId;
    }

    public BigInteger getBillerId() {
        return billerId;
    }

    public void setBillerId(BigInteger billerId) {
        this.billerId = billerId;
    }

    public BigInteger getShipperId() {
        return shipperId;
    }

    public void setShipperId(BigInteger shipperId) {
        this.shipperId = shipperId;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public Date getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
    }

    public Date getPickedupDate() {
        return pickedupDate;
    }

    public void setPickedupDate(Date pickedupDate) {
        this.pickedupDate = pickedupDate;
    }

    public BigInteger getBookingOfficeLocId() {
        return bookingOfficeLocId;
    }

    public void setBookingOfficeLocId(BigInteger bookingOfficeLocId) {
        this.bookingOfficeLocId = bookingOfficeLocId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getOrderRefNo() {
        return orderRefNo;
    }

    public void setOrderRefNo(String orderRefNo) {
        this.orderRefNo = orderRefNo;
    }

    public BigDecimal getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(BigDecimal totalWeight) {
        this.totalWeight = totalWeight;
    }

    public BigDecimal getTotalVolumentricWeight() {
        return totalVolumentricWeight;
    }

    public void setTotalVolumentricWeight(BigDecimal totalVolumentricWeight) {
        this.totalVolumentricWeight = totalVolumentricWeight;
    }

    public BigDecimal getTotalChargableWeight() {
        return totalChargableWeight;
    }

    public void setTotalChargableWeight(BigDecimal totalChargableWeight) {
        this.totalChargableWeight = totalChargableWeight;
    }

    public BigInteger getTotalShipmentCount() {
        return totalShipmentCount;
    }

    public void setTotalShipmentCount(BigInteger totalShipmentCount) {
        this.totalShipmentCount = totalShipmentCount;
    }

    public BigInteger getPincodeId() {
        return pincodeId;
    }

    public void setPincodeId(BigInteger pincodeId) {
        this.pincodeId = pincodeId;
    }

    public BigInteger getLeadId() {
        return leadId;
    }

    public void setLeadId(BigInteger leadId) {
        this.leadId = leadId;
    }

    public String getConsignmentStatus() {
        return consignmentStatus;
    }

    public void setConsignmentStatus(String consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

    public String getInsuranceInfo() {
        return insuranceInfo;
    }

    public void setInsuranceInfo(String insuranceInfo) {
        this.insuranceInfo = insuranceInfo;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getConsignmentMode() {
        return consignmentMode;
    }

    public void setConsignmentMode(String consignmentMode) {
        this.consignmentMode = consignmentMode;
    }

    public BigInteger getFranchiseeId() {
        return franchiseeId;
    }

    public void setFranchiseeId(BigInteger franchiseeId) {
        this.franchiseeId = franchiseeId;
    }

    public String getLastStatusReasonCode() {
        return lastStatusReasonCode;
    }

    public void setLastStatusReasonCode(String lastStatusReasonCode) {
        this.lastStatusReasonCode = lastStatusReasonCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getRescheduleDate() {
        return rescheduleDate;
    }

    public void setRescheduleDate(Date rescheduleDate) {
        this.rescheduleDate = rescheduleDate;
    }

    public BigInteger getUserId() {
        return userId;
    }

    public void setUserId(BigInteger userId) {
        this.userId = userId;
    }

    public BigDecimal getConsignmentAmount() {
        return consignmentAmount;
    }

    public void setConsignmentAmount(BigDecimal consignmentAmount) {
        this.consignmentAmount = consignmentAmount;
    }

    public String getConsignmentCurrency() {
        return consignmentCurrency;
    }

    public void setConsignmentCurrency(String consignmentCurrency) {
        this.consignmentCurrency = consignmentCurrency;
    }

    public BigDecimal getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(BigDecimal payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getPayableCurrency() {
        return payableCurrency;
    }

    public void setPayableCurrency(String payableCurrency) {
        this.payableCurrency = payableCurrency;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public BigInteger getParentConsignmentId() {
        return parentConsignmentId;
    }

    public void setParentConsignmentId(BigInteger parentConsignmentId) {
        this.parentConsignmentId = parentConsignmentId;
    }

    public BigInteger getParentPartnerId() {
        return parentPartnerId;
    }

    public void setParentPartnerId(BigInteger parentPartnerId) {
        this.parentPartnerId = parentPartnerId;
    }

    public BigInteger getRootConsignmentId() {
        return rootConsignmentId;
    }

    public void setRootConsignmentId(BigInteger rootConsignmentId) {
        this.rootConsignmentId = rootConsignmentId;
    }

    public String getRootWaybillNo() {
        return rootWaybillNo;
    }

    public void setRootWaybillNo(String rootWaybillNo) {
        this.rootWaybillNo = rootWaybillNo;
    }

    public BigInteger getRootPartnerId() {
        return rootPartnerId;
    }

    public void setRootPartnerId(BigInteger rootPartnerId) {
        this.rootPartnerId = rootPartnerId;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getConsignmentType() {
        return consignmentType;
    }

    public void setConsignmentType(String consignmentType) {
        this.consignmentType = consignmentType;
    }

    public String getDocketType() {
        return docketType;
    }

    public void setDocketType(String docketType) {
        this.docketType = docketType;
    }

    public String getProcessingType() {
        return processingType;
    }

    public void setProcessingType(String processingType) {
        this.processingType = processingType;
    }

    public String getFlowType() {
        return flowType;
    }

    public void setFlowType(String flowType) {
        this.flowType = flowType;
    }

    public Boolean getPodDone() {
        return isPodDone;
    }

    public void setPodDone(Boolean podDone) {
        isPodDone = podDone;
    }

    public Boolean getEligibleForRto() {
        return isEligibleForRto;
    }

    public void setEligibleForRto(Boolean eligibleForRto) {
        isEligibleForRto = eligibleForRto;
    }

    public String getReasonEligibleForRto() {
        return reasonEligibleForRto;
    }

    public void setReasonEligibleForRto(String reasonEligibleForRto) {
        this.reasonEligibleForRto = reasonEligibleForRto;
    }

    public Date getCustomerPromiseDate() {
        return customerPromiseDate;
    }

    public void setCustomerPromiseDate(Date customerPromiseDate) {
        this.customerPromiseDate = customerPromiseDate;
    }

    public BigInteger getStatusCorrectionRequestId() {
        return statusCorrectionRequestId;
    }

    public void setStatusCorrectionRequestId(BigInteger statusCorrectionRequestId) {
        this.statusCorrectionRequestId = statusCorrectionRequestId;
    }

    public BigInteger getParentShipperId() {
        return parentShipperId;
    }

    public void setParentShipperId(BigInteger parentShipperId) {
        this.parentShipperId = parentShipperId;
    }

    public Boolean isActive() {
        return isActive;
    }
}
