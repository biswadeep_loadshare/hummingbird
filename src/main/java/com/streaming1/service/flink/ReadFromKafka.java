package com.streaming1.service.flink;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.streaming1.entity.Consignments;
import com.streaming1.service.mysql.MysqlServiceNew;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.flink.streaming.util.serialization.KeyedSerializationSchema;
import org.apache.flink.util.Collector;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Properties;

//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.json.JSONTokener;

public class ReadFromKafka {

    static Logger logger = LoggerFactory.getLogger(Consignments.class);

    public static void main() throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        logger.info("stream environment created");

        Properties kafkaProps = new Properties();
        kafkaProps.setProperty("bootstrap.servers", "localhost:9092");
        kafkaProps.setProperty("zookeeper.connect", "localhost:2181");
        //kafka_props.setProperty("flink-test-group", "kconsumer");
        kafkaProps.setProperty("group.id", "flink-test-group");
        //kafkaProps.setProperty("AUTO_OFFSET_RESET_CONFIG", "earliest");

        logger.info("kafka configured");


        DataStream<String> kafkaStream = env.addSource(new FlinkKafkaConsumer011<>("foo", new SimpleStringSchema(), kafkaProps));

        //env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        logger.info("datasource added");

        DataStream<Consignments> gsonedKafkaStream;
        gsonedKafkaStream = kafkaStream.process(new ProcessFunction<String, Consignments>() {

            Gson gson = null;
            JSONObject jsonObj = null;
            JSONArray jsonArr = null;
            Consignments consignments = null;
            ArrayList<Consignments> consignmentsArray = null;

            @Override
            public void processElement(String kafkaStreamJSON, Context ctx, Collector<Consignments> out) throws Exception {

                try {
                    Object json = new JSONTokener(kafkaStreamJSON).nextValue();

                    if (json instanceof JSONObject) {
                        jsonObj = new JSONObject(kafkaStreamJSON);
                        consignments = gson.fromJson(jsonObj.toString(), Consignments.class);
                        logger.info("collecting data stream");
                        if (consignments != null)
                            out.collect(consignments);
                    } else if (json instanceof JSONArray) {
                        jsonArr = new JSONArray(kafkaStreamJSON);
                        consignmentsArray = gson.fromJson(jsonArr.toString(), new TypeToken<ArrayList<Consignments>>() {
                        }.getType());
                        logger.info("processing stream data");
                        for (int i = 0; i < consignmentsArray.size(); i++) {
                            if (consignmentsArray.get(i) != null)
                                out.collect(consignmentsArray.get(i));
                        }

                    }
                } catch (Exception e) {
                    logger.error("some error");
                    out.collect(null);
                }
            }


            @Override
            public void open(Configuration parameters) throws Exception {

                gson = new GsonBuilder().create();
                // logger.info("before keyby");

            }
        })
                .keyBy(new KeySelector<Consignments, String>() {

                    @Override
                    public String getKey(Consignments consignments) throws Exception {
                        return String.valueOf(consignments.getId());
                    }
                })


                //.map(new getPartner()).print();

                .process(new ProcessFunction<Consignments, Consignments>() {

                    @Override
                    public void processElement(Consignments consignments, Context context, Collector<Consignments> out) throws Exception {

                        Integer partnerId = consignments.getPartnerId();

                        String query = "select id, name from partners where id=" + partnerId + ";";

                        ResultSet rs = MysqlServiceNew.mysqlConnector(query);

                        String partnerName = null;

                        while (rs.next()) {
                            partnerName = rs.getString("name");
                        }

                        logger.info("enriched");

                        consignments.setPartnerName(partnerName);

                        out.collect(consignments);
                        //System.out.println(consignments);
                    }
                });

        gsonedKafkaStream.print();

        FlinkKafkaProducer011<Consignments> myProducer = new FlinkKafkaProducer011<Consignments>(
                "localhost:9092",
                "bar",
                new KeyedSerializationSchema<Consignments>() {
                    @Override
                    public byte[] serializeKey(Consignments consignments) {
                        return new byte[0];
                    }

                    @Override
                    public byte[] serializeValue(Consignments consignments) {
                        return new byte[0];
                    }

                    @Override
                    public String getTargetTopic(Consignments consignments) {
                        return null;
                    }
                }
        );

        gsonedKafkaStream.addSink(myProducer);

        System.out.println(env.getExecutionPlan());

        env.execute("Flink Kafka Stream");

    }
}




