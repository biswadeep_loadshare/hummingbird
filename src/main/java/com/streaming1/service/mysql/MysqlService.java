package com.streaming1.service.mysql;

import com.google.gson.Gson;
import com.streaming1.entity.Consignments;
import com.streaming1.service.kafkaServices.KafkaProducerService;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MysqlService {


    public static void mysqlConnector(String topic, String s) throws Exception {


        InputStream input = new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/application.properties");
        Properties prop = new Properties();
        prop.load(input);

        String dbname = prop.getProperty("mysql.dbname");
        String host = prop.getProperty("mysql.host");
        String user = prop.getProperty("mysql.user");
        String password = prop.getProperty("mysql.password");
        String port = prop.getProperty("mysql.port");

        // connection string

        String url = "jdbc:mysql://" + host + ":" + port + "/" + dbname;
        //System.out.println(url);
        Connection con = DriverManager.getConnection(url, user, password);
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(s);
        List<Consignments> consList = new ArrayList<Consignments>();

        while (rs.next()) {
            Consignments data = new Consignments();

            data.setId(rs.getString("id"));
            data.setWaybillNo(rs.getString("waybill_no"));
            data.setCreatedAt(rs.getTimestamp("created_at"));
            data.setPartnerId(Integer.valueOf(rs.getString("partner_id")));

            Gson gson = new Gson();
            String JSONOutput = gson.toJson(data);

            KafkaProducerService.kProducer(topic, JSONOutput);
        }

    }
}
