package com.streaming1.service.mysql;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class MysqlServiceNew {


    public static ResultSet mysqlConnector(String s) throws Exception {


        InputStream input = new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/application.properties");
        Properties prop = new Properties();
        prop.load(input);

        String dbname = prop.getProperty("mysql.dbname");
        String host = prop.getProperty("mysql.host");
        String user = prop.getProperty("mysql.user");
        String password = prop.getProperty("mysql.password");
        String port = prop.getProperty("mysql.port");

        // connection string
        String url = "jdbc:mysql://" + host + ":" + port + "/" + dbname;
        Connection con = DriverManager.getConnection(url, user, password);

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(s);

        return rs;

    }
}
