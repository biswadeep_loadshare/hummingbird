package com.streaming1.service.kafkaServices;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

public class KafkaConsumerService {

    public static void kConsumer() throws Exception{
        try (InputStream input = new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/application.properties")) {
            Properties props = new Properties();
            props.load(input);

            props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"localhost:9092");
            props.put(ConsumerConfig.GROUP_ID_CONFIG,"flink-test-group");
            props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");
            props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");

            KafkaConsumer<String,String> consumer = new KafkaConsumer<>(props);

            consumer.subscribe(Arrays.asList("foo")); // topics to subscribe to
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(100);
                for (ConsumerRecord<String, String> record : records)
                    System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
            }


        }
    }
}
