package com.streaming1.service.kafkaServices;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class KafkaProducerService {
    public static void kProducer(String topic, String value) throws InterruptedException {

        try (InputStream input = new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/application.properties")) {
            Properties props = new Properties();
            props.load(input);

            props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092"); //
            props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
            props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

            KafkaProducer<String,String> producer = new KafkaProducer<String, String>(props);

            // Keys are used to determine the partition within a log to which a message get's appended to.
            // While the value is the actual payload of the message.
            // If the key is null a random partition will the selected.
            // If the value is null it can have special "delete" semantics in case you enable log-compaction
            // instead of log-retention policy for a topic
            producer.send(new ProducerRecord<String, String >(topic,value));

            producer.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}