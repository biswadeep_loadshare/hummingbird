package com.streaming1.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EpochConvertor {
    public static long convertToEpoch(String s) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("MMM dd yyyy HH:mm:ss.SSS zzz");
        Date date = df.parse(String.valueOf(s));
        long epoch = date.getTime();
        return epoch;
    }

}
