package com.streaming1.main;

import com.streaming1.service.kafkaServices.KafkaConsumerService;
import com.streaming1.service.mysql.MysqlService;

public class EventGenerator {
    public static void main(String[] args) throws Exception {
        MysqlService.mysqlConnector("foo","select id, waybill_no, created_at, partner_id from consignments order by id desc limit 2;");
        KafkaConsumerService.kConsumer();
    }
}
